import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux'
import configureStore from './store'

import Routes from './routes';
import './App.css';

const store = configureStore()

const App = () => (
  <Provider store={store}>
    <Router>
      <Routes />
    </Router>
  </Provider>
)

export default App;
