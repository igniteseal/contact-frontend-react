import Home from './Home';
import Login from './Login';
import Profile from './Profile';
import Contacts from './Contacts';

export {
  Home,
  Login,
  Profile,
  Contacts,
}